import { ILog } from "./libs/ILog";

export class ConsoleLog implements ILog {
    info(message: string) : void {
        console.log(message);
    }
}