import { ConsoleLog } from "./ConsoleLog"

export class DOMLog extends ConsoleLog {
    constructor(private domElem: Element) {
        super();
        this.domElem = domElem;
    }

    info(message: string) : void {
        var newLine = document.createElement("div");
        newLine.innerHTML = message;
        this.domElem.appendChild(newLine);
        super.info(message);
    }
}