import { ILog } from "libs/ILog";

export class App {
    constructor(private log?: ILog) {
        this.log = log;
        log ? log.info(`${this.constructor.name} created at ${new Date()}`) : null;
    }
}