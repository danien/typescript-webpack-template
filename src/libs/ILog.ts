export interface ILog {
    info(message: string) : void;
}
