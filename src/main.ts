import * as App from "./app/app"
import { ConsoleLog } from "./ConsoleLog"
import { DOMLog } from "./DOMLog"

var output = document.getElementById("output");
var text = `Started at ${new Date()}`;
output.innerHTML = text;
console.log(text);

var domLog = new DOMLog(output);
var app = new App.App(domLog);
