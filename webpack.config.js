var path = require('path');

module.exports = {
  entry: './src/main',

  output: {
    path: path.resolve(__dirname, 'wwwroot/scripts'),
    filename: '[name]-bundle.js'
  },

  resolve: {
    extensions: ['.ts', '.js']
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: ['ts-loader']
      },
      {
        // enforce: 'pre',
        test: /\.js$/,
        use: ['source-map-loader']
      }
    ],
  },

  devtool: 'source-map',

  devServer: {
    watchContentBase: true,
    watchOptions: {
      poll: true
    },
    inline: true,
    contentBase: 'wwwroot',
    // publicPath is required for live reloading to work.
    // Changes are compiled in memory, not to files on disk, and served from publicPath.
    // To get changes compiled to files on disk, you need to run webpack as well.
    publicPath: '/scripts/',
    compress: true,
    port: 8080
  }
};